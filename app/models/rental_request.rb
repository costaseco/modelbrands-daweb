class RentalRequest < ActiveRecord::Base
  belongs_to :model
  has_one :brand, through: :model
end

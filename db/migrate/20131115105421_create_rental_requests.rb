class CreateRentalRequests < ActiveRecord::Migration
  def change
    create_table :rental_requests do |t|
      t.string :name
      t.string :phone
      t.integer :model_id

      t.timestamps
    end
  end
end

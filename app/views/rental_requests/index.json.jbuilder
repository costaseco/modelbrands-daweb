json.array!(@rental_requests) do |rental_request|
  json.extract! rental_request, :name, :phone, :model_id
  json.url rental_request_url(rental_request, format: :json)
  json.model rental_request.model.name
  json.brand rental_request.model.brand.name
end


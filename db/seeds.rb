# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Brand.delete_all
Model.delete_all

brands = [
  {:name => "Alfa Romeo", :models => ["Giulietta","MiTO"]},
  {:name => "Aston Martin", :models => ["Cygnet","DB9","DBS","Rapide","Vantage","Virage"]},
  {:name => "Audi", :models => ["A1","A3","A4","A4 Allroad","A5"]},
  {:name => "Bentley", :models => ["Continental","Continental GT"]}
]

brands.each do |b|
  b_obj = Brand.create(name: b[:name])
  b[:models].each do |m|
    Model.create(name:m, brand_id: b_obj.id)
  end
end


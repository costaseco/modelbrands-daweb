
// This is the Javascript file for the home views

$(loadRequests);

$(function() {
	$('#filter').keyup(loadRequests);
});

function loadRequests() {
	$('#requestlist').html("");
	$.getJSON('rental_requests.json',
			  { filter: $('#filter').val() },
			  function (data) {
			  	for( d in data ) {
			  		createLIRequest(data[d].name,data[d].model,data[d].brand);
			  	}	
			  });
}

function createLIRequest(name,model,brand) {
	var code = [];
	code.push($('<div>').text(name));
	code.push($('<div>').text(model));
	code.push($('<div>').text(brand));
	$('<li>').html(code).appendTo('#requestlist');
}

$(loadBrands);

function loadBrands() {
	$('#brands').html($('<option>',{value:0}).text('Select a brand'));
	$.getJSON('brands.json',
			  function (data) {
			  	for( d in data ) {
			  		$('<option>',{ value: data[d].id }).text(data[d].name).appendTo('#brands');
			  	}	
			  });
}


$(function() {
	$('#brands').change(loadModels);
});

function loadModels() {
	var brand_id = $('#brands > option:checked').val();

	if( brand_id == 0 ) {
		$('#models').hide();
	} else {
		$.getJSON('models.json',
				{ brand_id : brand_id },
			  function (data) {
		  		$('#models').html($('<option>',{value:0}).text('Select a model'));
			  	for( d in data ) {
			  		$('<option>',{ value: data[d].id }).text(data[d].name).appendTo('#models');
			  	}	
			  });
	}
}

$(function() {
	$('#models').change(showNamePhone);
});

function showNamePhone() {
	var brand_id = $('#brands > option:checked').val();
	var model_id = $('#models > option:checked').val();

	if( model_id == 0 || brand_id == 0 ) {
		$('#name_container').css({"display":"none"});
	} else {
		$('#name_container').css({"display":"block"});
	}
}















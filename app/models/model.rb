class Model < ActiveRecord::Base
  belongs_to :brand
  has_many :rental_requests
end
